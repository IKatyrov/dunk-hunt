namespace Infrastructure
{
    public interface IConstruct
    {
        void Construct();
    }
}