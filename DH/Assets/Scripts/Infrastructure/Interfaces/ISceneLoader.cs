using System;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface ISceneLoader
    {
        Task Load(string nameScene, Action onLoaded = null);
    }
}