using Infrastructure.States;

namespace Infrastructure
{
    public interface IGame
    {
        IGameStateMachine StateMachine { get; }
    }
}