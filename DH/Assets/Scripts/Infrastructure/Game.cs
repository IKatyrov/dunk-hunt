using Infrastructure.States;
using Zenject;

namespace Infrastructure
{
    public class Game : IGame
    {
        private readonly IGameStateMachine _stateMachine;

        public IGameStateMachine StateMachine => _stateMachine;

        public Game(DiContainer container)
        {
            _stateMachine = new GameStateMachine(new SceneLoader(), container);
        }
    }
}