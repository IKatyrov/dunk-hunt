using System.Threading.Tasks;
using UnityEngine;

namespace Infrastructure.AssetManagement
{
    public interface IAssetProvider
    {
        Task<GameObject> CreateInstanceAsync(string address);
        Task<GameObject> CreateInstanceAsync(string address, Vector3 pos);
        Task<T> LoadAsync<T>(string address);
        void CleanUpAsync();
    }
}