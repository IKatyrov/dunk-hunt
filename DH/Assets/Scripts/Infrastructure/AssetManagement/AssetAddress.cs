namespace Infrastructure.AssetManagement
{
    public static class AssetAddress
    {
        public const string Curtain = "Curtain";
        public const string PoolPreset = "PoolPresetBirds";
        public static string Inputcallback = "InputCallback";
        public static string UiController = "UiController";
    }
}