using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Infrastructure.AssetManagement
{
    public class AssetProvider : IAssetProvider
    {
        private readonly Dictionary<string, AsyncOperationHandle> _cachedHandlesAssets;
        private readonly Dictionary<string, List<AsyncOperationHandle>> _cachedHandlesInstance;
        public AssetProvider()
        {
            _cachedHandlesAssets = new Dictionary<string, AsyncOperationHandle>();
            _cachedHandlesInstance = new Dictionary<string, List<AsyncOperationHandle>>();
        }   

        public async Task<GameObject> CreateInstanceAsync(string address)
        {
            var handle = Addressables.InstantiateAsync(address);
            var go = await RunWithCacheOnCompleteInstance(address, handle);
            return go;
        }
        
        public async Task<GameObject> CreateInstanceAsync(string address, Vector3 pos)
        {
            var handle = Addressables.InstantiateAsync(address);
            var go = await RunWithCacheOnCompleteInstance(address, handle);
            go.transform.localPosition = pos;
            return go;
        }

        public async Task<T> LoadAsync<T>(string address)
        {
            if (_cachedHandlesAssets.TryGetValue(address, out AsyncOperationHandle completedHandle))
                return (T) completedHandle.Task.Result;

            var handle = Addressables.LoadAssetAsync<T>(address);
            _cachedHandlesAssets.Add(address, handle);
            return await handle.Task;
        }

        public void CleanUpAsync()
        {
            foreach (var handle in _cachedHandlesAssets.Values)
            {
                Addressables.Release(handle);
            }

            foreach (var listHandles in _cachedHandlesInstance.Values)
            {
                foreach (var handle in listHandles)
                {
                    Addressables.Release(handle);
                }

                listHandles.Clear();
            }


            _cachedHandlesAssets.Clear();
            _cachedHandlesInstance.Clear();
        }


        private async Task<T> RunWithCacheOnCompleteInstance<T>(string cacheKey, AsyncOperationHandle<T> handle)
        {
            if (_cachedHandlesInstance.TryGetValue(cacheKey, out List<AsyncOperationHandle> operationHandle) == false)
            {
                operationHandle = new List<AsyncOperationHandle>(16);
                _cachedHandlesInstance.Add(cacheKey, operationHandle);
            }

            operationHandle.Add(handle);
            return await handle.Task;
        }
    }

}