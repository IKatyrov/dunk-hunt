﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Infrastructure
{
    public class SceneLoader : ISceneLoader
    {
        public async Task Load(string nameScene, Action onLoaded = null)
        {
            await LoadSceneAsync(nameScene, onLoaded);
        }
        
        private async Task LoadSceneAsync(string nextScene, Action onLoaded = null)
        {
            if (SceneManager.GetActiveScene().name == nextScene)
            {
                onLoaded?.Invoke();
                return;
            }

            AsyncOperation wait = SceneManager.LoadSceneAsync(nextScene);

            while (wait.isDone == false) 
                await Task.Delay(1);

            onLoaded?.Invoke();

        }
    }
}