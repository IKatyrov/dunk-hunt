namespace Infrastructure
{
    public static class SceneName
    {
        public const string Initial = nameof(Initial);
        public const string Game = nameof(Game);
    }
}