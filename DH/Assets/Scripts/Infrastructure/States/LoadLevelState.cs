using System.Collections.Generic;
using System.Threading.Tasks;
using Bird.MVP;
using Infrastructure.AssetManagement;
using Infrastructure.Factory;
using Interfaces;
using NTC.Global.Pool;
using Ui;

namespace Infrastructure.States
{
    public class LoadLevelState : IPayloadedState<string>
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly IGameFactory _gameFactory;

        public LoadLevelState(IGameStateMachine stateMachine, ISceneLoader sceneLoader, IGameFactory gameFactory)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _gameFactory = gameFactory;
        }

        public async void Enter(string sceneName)
        {
            NightPool.Reset();
            _gameFactory.CleanUp();
            
            ILoadingCurtain loadingCurtain = await _gameFactory.CreateLoadingCurtain();
            loadingCurtain.Construct();
            loadingCurtain.Show();

            await _sceneLoader.Load(sceneName, OnLoaded);
            await loadingCurtain.Hide();
        }

        public void Exit()
        {
        }

        private async void OnLoaded()
        {
            await InitGameWorld();
            _stateMachine.Enter<GameLoopState>();
        }

        private async Task InitGameWorld()
        {
            IInputCallback inputCallback = await InitInputCallback();
            IUiController uiController = await InitUiController();
            ISpawnBird spawnBird = await InitSpawnBird(uiController);

            IGamePause gamePause = new GamePause(spawnBird, uiController);
            gamePause.AssignListeners();
            
            IMediatorListeners mediatorListeners = new MediatorListeners(inputCallback, uiController);
            mediatorListeners.AssignListeners();
            
            spawnBird.StartSpawningBird();
        }

        private async Task<IUiController> InitUiController() => 
            await _gameFactory.CreateUiController();

        private async Task<IInputCallback> InitInputCallback() => 
            await _gameFactory.CreateInputcalback();

        private async Task<ISpawnBird> InitSpawnBird(IUiController uiController)
        {
            var poolItemsGo = new List<BirdView>();
            var poolPreset = await InitPoolPresetBird();
            
            foreach (var poolItem in poolPreset.poolItems) 
                poolItemsGo.Add(poolItem.prefab.GetComponent<BirdView>());

            var spawnBird = new SpawnBird(poolItemsGo, uiController);
            return spawnBird;
        }

        private async Task<PoolPreset> InitPoolPresetBird()
        {
            var poolPreset = await _gameFactory.LoadAsset<PoolPreset>(AssetAddress.PoolPreset);
            await NightPool.InstallPoolItems(poolPreset);
            return poolPreset;
        }
    }
}