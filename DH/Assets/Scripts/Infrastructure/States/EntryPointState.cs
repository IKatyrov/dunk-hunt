using Infrastructure.AssetManagement;
using Infrastructure.Factory;
using Zenject;

namespace Infrastructure.States
{
    public class EntryPointState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly DiContainer _container;
        
        public EntryPointState(IGameStateMachine stateMachine, ISceneLoader sceneLoader, DiContainer container)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _container = container;

            RegisterService();
        }

        private void RegisterService()
        {
            _container.BindInstance<IAssetProvider>(new AssetProvider()).AsSingle();
            _container.BindInstance<IGameFactory>(new GameFactory(_container.Resolve<IAssetProvider>())).AsSingle();
        }
        
        public void Enter()
        {
           _sceneLoader.Load(SceneName.Initial, EnterLoadLevel);
        }

        public void Exit() { }

        private void EnterLoadLevel()
        {
            _stateMachine.Enter<LoadLevelState, string>(SceneName.Game);
        }
    }
}