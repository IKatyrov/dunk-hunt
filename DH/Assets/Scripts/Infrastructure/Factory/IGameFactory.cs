using System.Threading.Tasks;
using Interfaces;
using Ui;

namespace Infrastructure.Factory
{
    public interface IGameFactory
    {
        Task<ILoadingCurtain> CreateLoadingCurtain();
        Task<IInputCallback> CreateInputcalback();
        Task<IUiController> CreateUiController();
        Task<T> LoadAsset<T>(string address);
        void CleanUp();
    }
}