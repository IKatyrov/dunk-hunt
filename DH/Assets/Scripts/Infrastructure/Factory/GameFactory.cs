using System.Threading.Tasks;
using Infrastructure.AssetManagement;
using Interfaces;
using Ui;

namespace Infrastructure.Factory
{
    public class GameFactory : IGameFactory
    {
        private readonly IAssetProvider _assetsProvider;

        public GameFactory(IAssetProvider assetsProvider)
        {
            _assetsProvider = assetsProvider;
        }

        public async Task<ILoadingCurtain> CreateLoadingCurtain()
        {
            var go = await _assetsProvider.CreateInstanceAsync(AssetAddress.Curtain);
            return go.GetComponent<ILoadingCurtain>();
        }
        
        public async Task<IInputCallback> CreateInputcalback()
        {
            var go = await _assetsProvider.CreateInstanceAsync(AssetAddress.Inputcallback);
            return go.GetComponent<IInputCallback>();
        }

        public async Task<IUiController> CreateUiController()
        {
            var go = await _assetsProvider.CreateInstanceAsync(AssetAddress.UiController);
            return go.GetComponent<IUiController>();
        }

        public async Task<T> LoadAsset<T>(string address)
        {
            return await _assetsProvider.LoadAsync<T>(address);
        }

        public void CleanUp()
        {
            _assetsProvider.CleanUpAsync();
        }

    }
}