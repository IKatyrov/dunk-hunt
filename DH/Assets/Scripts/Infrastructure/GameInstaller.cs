using Infrastructure.States;
using Zenject;

namespace Infrastructure
{
    public class GameInstaller : MonoInstaller
    {
        private IGame _game;

        public override void InstallBindings()
        {
            _game = new Game(Container);
            _game.StateMachine.Enter<EntryPointState>();
        }
    }
}