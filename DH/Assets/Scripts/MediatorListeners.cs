using Interfaces;

public class MediatorListeners : IMediatorListeners
{
    private IInputCallback _inputCallback;
    private IUiController _uiController;

    public MediatorListeners(IInputCallback inputCallback, IUiController uiController)
    {
        _inputCallback = inputCallback;
        _uiController = uiController;
    }

    public void AssignListeners()
    {
        _inputCallback.AddListenerOnPointerDownObject(_uiController.PointObject);
        _inputCallback.AddListenerOnPointerMissed(_uiController.PointMissed);
    }

    public void DisposeListeners()
    {
        _inputCallback.RemoveListenerOnPointerDownObject(_uiController.PointObject);
        _inputCallback.RemoveListenerOnPointerMissed(_uiController.PointMissed);
    }
}
