using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bird;
using Bird.MVP;
using Interfaces;
using NTC.Global.Pool;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class SpawnBird : ISpawnBird
{
    private readonly List<BirdView> _birdViewPrefab;
    private readonly IUiController _uiController;
    private readonly Camera _camera;

    private float _cameraOffsetXbird = 0.6f;
    private float _cameraOffsetY = 0.5f;

    private Vector2 _bottomLeftCorner;
    private Vector2 _topRightCorner;

    private bool _isWorkingSpawner;

    private float PosX => _topRightCorner.x + _cameraOffsetXbird;
    private float PosY => Random.Range(_topRightCorner.y - _cameraOffsetY, _bottomLeftCorner.y + _cameraOffsetY);

    public SpawnBird(List<BirdView> birdViewPrefab, IUiController uiController)
    {
        _birdViewPrefab = birdViewPrefab;
        _uiController = uiController;
        
        _camera = Camera.main;

        if (_camera == null)
            throw new InvalidOperationException($"Non camera on scene {SceneManager.GetActiveScene().name}");
        
        _bottomLeftCorner = _camera.ViewportToWorldPoint(Vector2.zero);
        _topRightCorner = _camera.ViewportToWorldPoint(Vector2.one);
    }

    public async void StartSpawningBird()
    {
        _isWorkingSpawner = true;
        await CreateBirdAsync();
    }

    public void StopSpawningBird()
    {
        _isWorkingSpawner = false;
    }

    private void InitBird()
    {
        int randomBird = Random.Range(0, _birdViewPrefab.Count);
        BirdView bird = NightPool.Spawn(_birdViewPrefab[randomBird]);
        SetPositionBirdView(bird);
        InjectUiToBird(bird);
    }

    private void InjectUiToBird(BirdView bird)
    {
        bird.BirdRenderer.InjectUiController(_uiController);
    }

    private void SetPositionBirdView(BirdView bird)
    {
        if (bird.BirdRenderer.DirectionBird == Direction.Left)
            SetSpawnPositionBirdView(bird, PosX, PosY);
        else
            SetSpawnPositionBirdView(bird, -PosX, PosY);
    }

    private void SetSpawnPositionBirdView(BirdView bird, float xPos, float yPos)
    {
        bird.transform.localPosition = new Vector3(xPos, yPos, 0);
    }

    private async Task CreateBirdAsync()
    {
        while (_isWorkingSpawner)
        {
            InitBird();
            float timeIntervalBetweenCreation = Random.Range(1f, 5f);
            await Task.Delay(TimeSpan.FromSeconds(timeIntervalBetweenCreation));
        }
    }
}
