﻿namespace Interfaces
{
    public interface ISpawnBird
    {
        void StartSpawningBird();
        void StopSpawningBird();
    }
}