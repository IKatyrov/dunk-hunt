﻿namespace Interfaces
{
    public interface IGameOverUI
    {
        void ActivatedPanelGameOver();
        void DeactivatedPanelGameOver();
    }
}