﻿using System;

namespace Interfaces
{
    public interface IInputCallback
    {
        void AddListenerOnPointerDownObject(Action listener);
        void AddListenerOnPointerMissed(Action listener);
        void RemoveListenerOnPointerDownObject(Action listener);
        void RemoveListenerOnPointerMissed(Action listener);
    }
}