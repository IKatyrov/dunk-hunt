﻿using System;
using Interfaces;
using UnityEngine;

public class InputCallback : MonoBehaviour, IInputCallback
{
    private Camera _camera;
    private event Action PointerDownObjectEvent;
    private event Action PointerMissedEvent;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        InputPointerPosition();
    }

    private void InputPointerPosition()
    {
        if (Input.GetMouseButtonDown(0))
            CallActionOnPointer();
    }

    private void CallActionOnPointer()
    {
        var pointerObject = GetPointObject();

        if (pointerObject == null)
            PointerMissedEvent?.Invoke();
        else
            PointerDownObjectEvent?.Invoke();
    }

    private GameObject GetPointObject()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit2D = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        GameObject objectToReturn = null;

        if (hit2D.collider != null)
            objectToReturn = hit2D.collider.gameObject;

        return objectToReturn;
    }

    public void AddListenerOnPointerDownObject(Action listener)
    {
        PointerDownObjectEvent += listener;
    }

    public void AddListenerOnPointerMissed(Action listener)
    {
        PointerMissedEvent += listener;
    }

    public void RemoveListenerOnPointerDownObject(Action listener)
    {
        PointerDownObjectEvent -= listener;
    }
    
    public void RemoveListenerOnPointerMissed(Action listener)
    {
        PointerMissedEvent -= listener;
    }
}
