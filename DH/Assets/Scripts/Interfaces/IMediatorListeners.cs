public interface IMediatorListeners
{
    void AssignListeners();
    void DisposeListeners();
}