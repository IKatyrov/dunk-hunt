public interface IGamePause
{
    void AssignListeners();
    void DisposeListeners();
}