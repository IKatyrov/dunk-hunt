using Interfaces;
using UnityEngine;

public class GamePause : IGamePause
{
    private ISpawnBird _spawnBird;
    private IUiControllerListeners _uiControllerListeners;
    private IUiControllerElements _uiControllerElements;

    public GamePause(ISpawnBird spawnBird, IUiController uiController)
    {
        _spawnBird = spawnBird;
        _uiControllerListeners = (IUiControllerListeners)uiController;
        _uiControllerElements = (IUiControllerElements)uiController;
    }

    public void AssignListeners()
    {
        _uiControllerListeners.AddListenerOnGameOver(PauseGame);
        _uiControllerListeners.AddListenerRestartGame(ResumeGame);
    }

    public void DisposeListeners()
    {
        _uiControllerListeners.RemoveListenerOnGameOver(PauseGame);
        _uiControllerListeners.RemoveListenerRestartGame(ResumeGame);
    }

    private void ResumeGame()
    {
        _spawnBird.StartSpawningBird();
        SetSettingsUI();
        Time.timeScale = 1;
    }

    private void PauseGame()
    {
        _spawnBird.StopSpawningBird();
        _uiControllerElements.TimerUI.StopTimer();
        Time.timeScale = 0;
    }

    private void SetSettingsUI()
    {
        _uiControllerElements.ScoreUI.ResetStartScoreValue();
        _uiControllerElements.LineUI.ResetFillLine();
        _uiControllerElements.TimerUI.ResetStartTimer();
        _uiControllerElements.TimerUI.StartTimer();
    }
}

