using System.Threading.Tasks;
using UnityEngine;

namespace Ui
{
    [RequireComponent(typeof(CanvasGroup))]
    public class LoadingCurtain : MonoBehaviour, ILoadingCurtain
    {
        private CanvasGroup _curtain;

        public void Construct()
        {
            DontDestroyOnLoad(this);
            _curtain = GetComponent<CanvasGroup>();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _curtain.alpha = 1;
        }

        public async Task Hide()
        {
            await DoFadeInAsync();
        }

        private async Task DoFadeInAsync()
        {
            while (_curtain.alpha > 0)
            {
                _curtain.alpha -= 0.03f;
                await Task.Delay(30);
            }
            gameObject.SetActive(false);
        }
    }
}