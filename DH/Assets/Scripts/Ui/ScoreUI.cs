using Interfaces;
using TMPro;
using UnityEngine;

namespace Ui
{
    public class ScoreUI : MonoBehaviour, IScoreUI
    {
        [SerializeField] private int _currentScoreValue;
        private const int _startScoreValue = 10;
  
        [SerializeField] private TextMeshProUGUI _scoreText;
        private IUiController _uiController;

        private void Start()
        {
            _uiController = GetComponentInParent<IUiController>();
            ResetStartScoreValue();
        }

        public void ResetStartScoreValue()
        {
            if (_currentScoreValue < _startScoreValue)
                _currentScoreValue = _startScoreValue;

            _scoreText.text = _currentScoreValue.ToString();
        }

        public void IncreaseScore()
        {
            _currentScoreValue++;
            _scoreText.text = _currentScoreValue.ToString();
        }

        public void DecreaseScore()
        {
            _currentScoreValue--;       

            if (_currentScoreValue <= 0)        
            {
                _uiController.GameOver();
                _currentScoreValue = 0;
            }

            _scoreText.text = _currentScoreValue.ToString();
        }
    }
}
