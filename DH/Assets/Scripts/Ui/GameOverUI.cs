using Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class GameOverUI : MonoBehaviour, IGameOverUI
    {
        [SerializeField] private Button _restartBtn;
        [SerializeField] private Button _quitBtn;
    
        private IUiController _uiController;

        private void Start()
        {
            _uiController = GetComponentInParent<IUiController>();
            AssignGameOverUiListener();
            DeactivatedPanelGameOver();
        }

        private void AssignGameOverUiListener()
        {
            _restartBtn.onClick.AddListener(RestartGame);
            _quitBtn.onClick.AddListener(AppQuit);
        }

        private void AppQuit()
        {
            Application.Quit();
        }

        private void RestartGame()
        {
            _uiController.ResumeGame();
        }

        public void ActivatedPanelGameOver()
        {
            gameObject.SetActive(true);
        }

        public void DeactivatedPanelGameOver()
        {
            gameObject.SetActive(false);
        }
    }
}
