using System;
using Interfaces;
using UnityEngine;

namespace Ui
{
    public class UiController : MonoBehaviour, IUiController, IUiControllerListeners, IUiControllerElements
    {
        private const string Canvas = nameof(Canvas);
        private const string @default = nameof(@default);

        private ILineUI _lineUI;
        private ITimerUI _timerUI;
        private IScoreUI _scoreUI;
        private IGameOverUI _gameOverUI;

        private Canvas _canvasObject;
        
        private event Action GameOverEvent;
        private event Action ResumeGameEvent;

        public ILineUI LineUI { get => _lineUI; }
        public ITimerUI TimerUI { get => _timerUI; }
        public IScoreUI ScoreUI { get => _scoreUI; }

        private void Start()
        {
            PreapreUiComponents();
        }

        private void PreapreUiComponents()
        {
            _canvasObject = GetComponent<Canvas>();
            _canvasObject.worldCamera = Camera.main;
            
            _lineUI = GetComponentInChildren<ILineUI>();
            _timerUI = GetComponentInChildren<ITimerUI>();
            _scoreUI = GetComponentInChildren<IScoreUI>();
            _gameOverUI = GetComponentInChildren<IGameOverUI>();
        }

        public void PointObject()
        {
            IncreaseFillLine();
            IncreaseScore();
        }

        public void PointMissed()
        {
            ResetFillLine();
        }

        private void IncreaseScore()
        {
            _scoreUI?.IncreaseScore();
        }

        public void DecreaseScore()
        {
            _scoreUI?.DecreaseScore();
        }

        private void IncreaseFillLine()
        {
            _lineUI?.IncreaseFillLine();
        }

        public void ResetFillLine()
        {
            _lineUI?.ResetFillLine();
        }

        public void IncreaseTime()
        {
            _timerUI?.IncreaseTime();
        }

        public void DecreaseTime()
        {
            _timerUI?.DecreaseTime();
        }

        public void GameOver()
        {
            _canvasObject.sortingLayerName = Canvas;
            _gameOverUI.ActivatedPanelGameOver();
            GameOverEvent?.Invoke();
        }

        public void ResumeGame()
        {
            _canvasObject.sortingLayerName = @default;
            _gameOverUI.DeactivatedPanelGameOver();
            ResumeGameEvent?.Invoke();
        }

        public void AddListenerOnGameOver(Action listener)
        {
            GameOverEvent += listener;
        }

        public void AddListenerRestartGame(Action listener)
        {
            ResumeGameEvent += listener;
        }

        public void RemoveListenerOnGameOver(Action listener)
        {
            GameOverEvent -= listener;
        }

        public void RemoveListenerRestartGame(Action listener)
        {
            ResumeGameEvent -= listener;
        }
    }
}
