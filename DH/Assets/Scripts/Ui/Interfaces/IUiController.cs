﻿namespace Interfaces
{
    public interface IUiController
    {
        void PointObject();
        void PointMissed();
        void DecreaseScore();
        void ResetFillLine();
        void IncreaseTime();
        void DecreaseTime(); 
        void GameOver();
        void ResumeGame();
    }
}