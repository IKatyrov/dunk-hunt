﻿namespace Interfaces
{
    public interface IUiControllerElements
    {
        ILineUI LineUI { get; }
        IScoreUI ScoreUI { get; }
        ITimerUI TimerUI { get; }
    }
}