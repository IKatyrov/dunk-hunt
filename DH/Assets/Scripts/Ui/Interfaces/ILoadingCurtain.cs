using System.Threading.Tasks;
using Infrastructure;

namespace Ui
{
    public interface ILoadingCurtain : IConstruct
    {
        void Show();
        Task Hide();
    }
}