﻿namespace Interfaces
{
    public interface IScoreUI
    {
        void ResetStartScoreValue();
        void DecreaseScore();
        void IncreaseScore();
    }
}