﻿using System;

namespace Interfaces
{
    public interface IUiControllerListeners
    {
        void AddListenerOnGameOver(Action listener);
        void AddListenerRestartGame(Action listener);
        void RemoveListenerOnGameOver(Action listener);
        void RemoveListenerRestartGame(Action listener);
    }
}