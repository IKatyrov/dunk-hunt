﻿namespace Interfaces
{
    public interface ITimerUI
    {
        public void StartTimer();
        public void StopTimer();
        void ResetStartTimer();
        void DecreaseTime();
        void IncreaseTime();
    }
}