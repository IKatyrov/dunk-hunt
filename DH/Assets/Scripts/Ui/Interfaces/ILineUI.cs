﻿namespace Interfaces
{
    public interface ILineUI
    {
        void IncreaseFillLine();
        void ResetFillLine();
    }
}