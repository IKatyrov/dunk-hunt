﻿using System.Collections;
using Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class LineUI : MonoBehaviour, ILineUI
    {
        private IUiController uiController;
        private Slider fillAmount;

        private const float recoveryRate = 0.25f;
        private const float stepIncrease = 0.2f;

        private void Start()
        {
            PrepareComponents();
            fillAmount.value = 0f;
        }

        private void PrepareComponents()
        {
            uiController = GetComponentInParent<IUiController>();
            fillAmount = GetComponentInChildren<Slider>();
        }

        private IEnumerator SmoothValue(float from, float to, float timer)
        {
            float t = 0.0f;
            while (t < 1.0f)
            {
                t += Time.deltaTime * (1.0f / timer);
                fillAmount.value = Mathf.Lerp(from, to, t);
                yield return 0;
            }

            if (fillAmount.value >= 1f)
            {
                uiController.IncreaseTime();
                uiController.ResetFillLine();
            }
        }

        public void IncreaseFillLine()
        {
            StartCoroutine(SmoothValue(fillAmount.value, fillAmount.value + stepIncrease, recoveryRate));
        }

        public void ResetFillLine()
        {
            StartCoroutine(SmoothValue(fillAmount.value, 0, recoveryRate));
        }
    }
}
