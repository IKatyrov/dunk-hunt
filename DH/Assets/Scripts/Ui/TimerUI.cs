using System.Collections;
using Interfaces;
using TMPro;
using UnityEngine;

namespace Ui
{
    public class TimerUI : MonoBehaviour, ITimerUI
    {
        [SerializeField] private TextMeshProUGUI _timer;
        [SerializeField] private int _currentTime = 0;

        private IUiController _uiController;
    
        private const int _startTime = 30;
        private const int _stepIncreaseTime = 3;
        private bool _isWorkingCoroutine;

        private void Start()
        {
            _uiController = GetComponentInParent<IUiController>();
            ResetStartTimer();
            StartTimer();
        }

        public void StartTimer()
        {
            _isWorkingCoroutine = true;
            StartCoroutine(DecreaseTimeCoroutine());
        }

        public void StopTimer()
        {
            _isWorkingCoroutine = false;
            StopCoroutine(DecreaseTimeCoroutine());
        }

        public void ResetStartTimer()
        {
            if (_currentTime < _startTime)
                _currentTime = _startTime;

            _timer.text = _currentTime.ToString();
        }

        public void IncreaseTime()
        {
            _currentTime += _stepIncreaseTime;
            _timer.text = _currentTime.ToString();
        }

        public void DecreaseTime()
        {
            _currentTime--;
            _timer.text = _currentTime.ToString();

            if (_currentTime <= 0)
                _uiController.GameOver();
        }

        private IEnumerator DecreaseTimeCoroutine()
        {
            while (_isWorkingCoroutine)
            {
                yield return new WaitForSeconds(1f);
                _uiController.DecreaseTime();
            }
        }

    }
}
