using System;
using System.Threading.Tasks;
using Bird.MVP;
using UnityEngine;

namespace Bird.States
{
    public class GetHitState : State
    {
        private const float DelayBeforeFlight = 0.36f;
        private const float DelayBeforeDie = 1f;
        
        private bool IsFlying => BirdModel.CheckCurrentAnimation(AnimationType.Fly);

        public GetHitState(IModel birdModel) : base(birdModel) { }

        protected override void EnterState()
        {
            BirdModel.TakeDamage();
            ChangeAnimation();
        }

        private void ChangeAnimation()
        {
            if (IsFlying == false) 
                return;

            if (BirdModel.IsAlive)
                ActionModelClick(AnimationType.Hit, DelayBeforeChangeFly());
            else
                ActionModelClick(AnimationType.Die, DelayBeforeDestroy());
        }

        private async void ActionModelClick(AnimationType animationType, Task task)
        {
            BirdModel.SetAnimation(animationType);
            await task;
        }

        private async Task DelayBeforeChangeFly()
        {
            await Task.Delay(TimeSpan.FromSeconds(DelayBeforeFlight));
            BirdModel.Flying();
        }

        private async Task DelayBeforeDestroy()
        {
            BirdModel.SetVelocity(Vector2.zero);
            await Task.Delay(TimeSpan.FromSeconds(DelayBeforeDie));
            BirdModel.Died();
        }
    }
}