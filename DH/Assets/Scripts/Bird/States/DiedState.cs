using Bird.MVP;

namespace Bird.States
{
    public class DiedState : State
    {
        public DiedState(IModel birdModel) : base(birdModel) { }

        protected override void EnterState()
        {
            BirdModel.ReturnToPool();
        }
    }
}