using Bird.MVP;

namespace Bird.States
{
    public abstract class State : IState
    {
        protected readonly IBirdModel BirdModel;

        protected State(IModel birdModel)
        {
            BirdModel = (IBirdModel)birdModel;
        }

        public void Enter() => 
            EnterState();

        public void Exit() => 
            ExitState();


        protected virtual void EnterState() { }
        protected virtual void ExitState() { }
        public virtual void StateUpdate() { }
    }
}