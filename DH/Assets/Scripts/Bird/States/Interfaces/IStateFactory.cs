namespace Bird.States
{
    public interface IStateFactory
    {
        IState GetState(StateType stateType);
    }
}