namespace Bird.States
{
    public interface IState
    {
        void Enter();
        void Exit();
        void StateUpdate();
    }
}