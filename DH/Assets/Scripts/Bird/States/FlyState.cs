using Bird.MVP;
using UnityEngine;

namespace Bird.States
{
    public class FlyState : State
    {
        public FlyState(IModel birdModel) : base(birdModel) { }

        protected override void EnterState()
        {
            BirdModel.SetAnimation(AnimationType.Fly);
        }

        public override void StateUpdate()
        {
            Vector2 currentVelocity = BirdModel.MovementData.CurrentVelocity;
            BirdModel.SetVelocity(currentVelocity);
        }
    }
}