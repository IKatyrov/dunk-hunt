﻿using System.Collections.Generic;
using Bird.MVP;

namespace Bird.States
{
    public class StateFactory : IStateFactory
    {
        private readonly Dictionary<StateType, State> _states;

        public StateFactory(IModel birdModel)
        {
            _states = new Dictionary<StateType, State>
            {
                {StateType.Fly, new FlyState(birdModel)},
                {StateType.GetHit, new GetHitState(birdModel)},
                {StateType.Die, new DiedState(birdModel)},
            };
        }

        public IState GetState(StateType stateType) => 
            _states[stateType];
    }

    public enum StateType
    {
        Fly, GetHit, Die
    }
    
}