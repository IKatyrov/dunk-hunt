using UnityEngine;

namespace Bird
{
    public class MovementData : IMovementData
    {
        private readonly float _minSpeed;
        private readonly float _maxSpeed;

        private int _horizontalMovementDirection;
        private float _currentSpeed;
        
        private Vector2 _currentVelocity;
        private Direction _movementVector;

        public Vector2 CurrentVelocity => _currentVelocity;
        public Direction MovementVector => _movementVector;

        public MovementData(float minSpeed, float maxSpeed)
        {
            _minSpeed = minSpeed;
            _maxSpeed = maxSpeed;
        }

        public void InitData()
        {
            _movementVector = CalculateVector();
            _horizontalMovementDirection = CalculateHorizontalDirection();
            _currentSpeed = CalculateCurrentSpeed();
            _currentVelocity = CalculateVelocity();
        }

        private Direction CalculateVector() => 
            (Direction)Random.Range(0, 2);

        private int CalculateHorizontalDirection() => 
            MovementVector == Direction.Right ? 1 : -1;

        private float CalculateCurrentSpeed() => 
            Random.Range(_minSpeed, _maxSpeed);

        private Vector2 CalculateVelocity() => 
            Vector3.right * _horizontalMovementDirection * _currentSpeed;
    }
}