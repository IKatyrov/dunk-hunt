﻿using System.Collections.Generic;
using UnityEngine;

namespace Bird
{
    public class BirdAnimator : IBirdAnimator
    {
        private const string Flying = nameof(Flying);
        private const string Hit  = nameof(Hit);
        private const string Die  = nameof(Die);
        
        private readonly Animator _animator;
        private readonly Dictionary<AnimationType, string> _animations;

        public BirdAnimator(Animator animator)
        {
            _animator = animator;
            
            _animations = new Dictionary<AnimationType, string>
            {
                {AnimationType.Fly, Flying},
                {AnimationType.Hit, Hit},
                {AnimationType.Die, Die},
            };
        }

        public void PlayAnimation(AnimationType animationType)
        {
            string animationName = _animations[animationType];
            Play(animationName);
        }

        public bool CheckCurrentAnimation(AnimationType animationType)
        {
            string animationName = _animations[animationType];
            return _animator.GetCurrentAnimatorStateInfo(0).IsName(animationName);
        }

        private void Play(string animationName) => 
            _animator.Play(animationName, 0, 0f);
    }
    
    public enum AnimationType
    {
        Fly, Hit, Die
    }
}