﻿using System;
using NTC.Global.Pool;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bird.MVP
{
    public class BirdView : MonoBehaviour, IPointerDownHandler, IBirdView
    {
        [SerializeField] private int _startHealth;
        private Animator _animatorCompoenent;
        private IPresenter _presenter;

        public IBirdAnimator BirdAnimator { get; private set; }
        public IBirdRenderer BirdRenderer { get; private set; }
        public Rigidbody2D Rb2D { get; private set; }
        public int StartHealth => _startHealth;
        public event Action GetHit;
        public event Action Updated;
        
        private void Awake()
        {
            Rb2D = GetComponent<Rigidbody2D>();
            
            _animatorCompoenent = GetComponentInChildren<Animator>();
            BirdRenderer = GetComponentInChildren<IBirdRenderer>();
            
            BirdAnimator = new BirdAnimator(_animatorCompoenent);
            _presenter = new BirdPresenter(this);
        }

        private void OnEnable() => 
            _presenter.Enable();

        private void OnDisable() => 
            _presenter.Disable();

        private void Update() => 
            Updated?.Invoke();

        private void OnValidate()
        {
            if (_startHealth <= 0)
                _startHealth = 1;
        }

        public void OnPointerDown(PointerEventData eventData) => 
            GetHit?.Invoke();

        public bool CheckCurrentAnimation(AnimationType animationType) => 
            BirdAnimator.CheckCurrentAnimation(animationType);

        public void ReturnToPool() => 
            NightPool.Despawn(gameObject);
    }
}