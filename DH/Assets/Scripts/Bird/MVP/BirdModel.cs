using System;
using Bird.States;
using UnityEngine;

namespace Bird.MVP
{
    public class BirdModel : IBirdModel
    {
        private readonly IBirdView _birdView;
        private readonly IMovementData _movementData;
        
        private const float MinSpeed = 1f;
        private const float MaxSpeed = 5f;
        
        private int _currentHealth;

        public event Action<StateType> StateChanged;
        public bool IsAlive => _currentHealth > 0;
        public IMovementData MovementData => _movementData;

        public BirdModel(IBirdView birdView)
        {
            _birdView = birdView;
            _movementData = new MovementData(MinSpeed, MaxSpeed);
        }
        
        public void InitCharacteristics()
        {
            _currentHealth = _birdView.StartHealth;
            _movementData.InitData();
            _birdView.BirdRenderer.SetFaceDirection(_movementData.MovementVector);
        }

        public void Flying() => 
            StateChanged?.Invoke(StateType.Fly);

        public void GetHit() => 
            StateChanged?.Invoke(StateType.GetHit);

        public void Died() => 
            StateChanged?.Invoke(StateType.Die);

        public void SetVelocity(Vector2 newVelocity) => 
            _birdView.Rb2D.velocity = newVelocity;

        public void TakeDamage() => 
            _currentHealth--;

        public void SetAnimation(AnimationType animationType) => 
            _birdView.BirdAnimator.PlayAnimation(animationType);

        public bool CheckCurrentAnimation(AnimationType animationType) =>
            _birdView.CheckCurrentAnimation(animationType);

        public void ReturnToPool() =>
            _birdView.ReturnToPool();
    }
}