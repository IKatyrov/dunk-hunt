using UnityEngine;

namespace Bird.MVP
{
    public interface IBirdModel : IModel
    {
        bool IsAlive { get; }
        IMovementData MovementData { get; }
        void InitCharacteristics();
        void Flying();
        void GetHit();
        void Died();
        void TakeDamage();
        void ReturnToPool();
        void SetVelocity(Vector2 newVelocity);
        void SetAnimation(AnimationType animationType);
        bool CheckCurrentAnimation(AnimationType animationName);
    }
}