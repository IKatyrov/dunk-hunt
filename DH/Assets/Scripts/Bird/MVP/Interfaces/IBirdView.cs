using UnityEngine;

namespace Bird.MVP
{
    public interface IBirdView : IView
    {
        IBirdAnimator BirdAnimator { get; }
        IBirdRenderer BirdRenderer { get; }
        Rigidbody2D Rb2D { get; }
        int StartHealth { get; }
        void ReturnToPool();
        bool CheckCurrentAnimation(AnimationType animationType);
    }
}