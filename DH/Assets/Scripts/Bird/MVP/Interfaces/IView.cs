using System;

namespace Bird.MVP
{
    public interface IView
    {
        event Action GetHit;
        event Action Updated;
    }
}