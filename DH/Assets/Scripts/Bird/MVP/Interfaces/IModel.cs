using System;
using Bird.States;

namespace Bird.MVP
{
    public interface IModel
    {
        event Action<StateType> StateChanged;
    }
}