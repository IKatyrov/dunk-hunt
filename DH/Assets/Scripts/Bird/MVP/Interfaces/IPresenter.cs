namespace Bird.MVP
{
    public interface IPresenter
    {
        void Enable();
        void Disable();
    }
}