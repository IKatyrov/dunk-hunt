using Bird.States;

namespace Bird.MVP
{
    public class BirdPresenter : IPresenter
    {
        private readonly IBirdView _birdView;
        private readonly IBirdModel _birdModel;
        private readonly IStateFactory _stateFactory;

        private IState _state;

        public BirdPresenter(IBirdView birdView)
        {
            _birdView = birdView;
            _birdModel = new BirdModel(birdView);
            _stateFactory = new StateFactory(_birdModel);
        }

        public void Enable()
        {
            _birdView.GetHit += OnGetHit;
            _birdView.Updated += OnUpdated;
            _birdModel.StateChanged += OnStateChanged;

            _birdModel.InitCharacteristics();
            _birdModel.Flying();
        }

        public void Disable()
        {
            _birdView.GetHit -= OnGetHit;
            _birdView.Updated -= OnUpdated;
            _birdModel.StateChanged -= OnStateChanged;
        }

        private void OnGetHit() =>
            _birdModel.GetHit();

        private void OnUpdated() => 
            _state.StateUpdate();

        private void OnStateChanged(StateType stateType)
        {
            _state?.Exit();
            _state = _stateFactory.GetState(stateType);
            _state.Enter();
        }
    }
}