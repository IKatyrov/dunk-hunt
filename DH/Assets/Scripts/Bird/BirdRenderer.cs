using Bird.MVP;
using Interfaces;
using UnityEngine;

namespace Bird
{
    public class BirdRenderer : MonoBehaviour, IBirdRenderer
    {
        public Direction DirectionBird { get; private set; }
        private bool _isVisible = true;
        private IUiController _uiController;
        
        public void SetFaceDirection(Direction direction)
        {
            DirectionBird = direction;
            
            if (direction == Direction.Left)
                transform.parent.localScale = new Vector3(-1 * Mathf.Abs(transform.parent.localScale.x), transform.parent.localScale.y, transform.parent.localScale.z);
            else
                transform.parent.localScale = new Vector3(Mathf.Abs(transform.parent.localScale.x), transform.parent.localScale.y, transform.parent.localScale.z);
        }

        public void InjectUiController(IUiController uiController)
        {
            _uiController = uiController;
        }
        
        private void OnBecameVisible()
        {
            _isVisible = true;
        }

        private void OnBecameInvisible()
        {
            if (_isVisible)
            {
                _isVisible = false;
                DegreaseScore();
                ReturnToPool();
            }
        }

        private void ReturnToPool()
        {
            BirdView bird = GetComponentInParent<BirdView>();
            bird.ReturnToPool();
        }

        private void DegreaseScore()
        {
            _uiController.DecreaseScore();
            _uiController = null;
        }
    }
}


