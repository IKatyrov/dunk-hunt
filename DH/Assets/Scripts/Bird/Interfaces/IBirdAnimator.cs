namespace Bird
{
    public interface IBirdAnimator
    {
        void PlayAnimation(AnimationType animationType);
        bool CheckCurrentAnimation(AnimationType animationType);
    }
}