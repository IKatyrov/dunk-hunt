using UnityEngine;

namespace Bird
{
    public interface IMovementData
    {
        Vector2 CurrentVelocity { get; }
        Direction MovementVector { get; }
        void InitData();
    }
}