using Interfaces;

namespace Bird
{
    public interface IBirdRenderer
    {
        Direction DirectionBird { get; }
        void SetFaceDirection(Direction direction);
        void InjectUiController(IUiController uiController);
    }
}