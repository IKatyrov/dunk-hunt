# Реплика duck hunt

## Требования к приложению

* Из-за краев экрана вылетают объекты в разное время с разной скоростью. Объекты должны быть анимированными.
* При нажатии на объект он уничтожается с анимацией.
* За каждый уничтоженный объект добавляется одно очко, если объект вылетел за противоположный край экрана - вычитать одно очко.
* В верхней части экрана должна быть полоска, которая заполняется, когда игрок попадает по цели (если промахнулся, то полоска сбрасывается). Если игрок попал 5 раз подряд (полоска заполнилась), то полоска сбрасывается.
* На игру дается 30 секунд, начальное количество очков - 10.
* Игра заканчивается, когда закончатся очки или время.

## Референс

https://disk.yandex.ru/i/Ooy_pQ3_3UEyXi


  > Иногда код будет редактироваться, а может и не будет))
